import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class BotService {

  constructor(private http: HttpClient) { }

  startSession(userId) {
    return this.http.post(`https://w5scq8xokb.execute-api.us-east-1.amazonaws.com/dev/bot/openSession/${userId}`, null);
  }

  getBlogsAndPosts(location) {
    return this.http.get(`https://w5scq8xokb.execute-api.us-east-1.amazonaws.com/dev/locations/${location}`)
  }

  postUserMessage(message) {
    const body = {
      "userInput" : message
    }
    return this.http.post(`https://w5scq8xokb.execute-api.us-east-1.amazonaws.com/dev/bot/userInput/${localStorage.getItem('userId')}`, body);
  }

  getRandomPostPic() {
    return this.http.get('https://api.unsplash.com/photos/?orientation=squarish&query=travel&page=2&per_page=20&client_id=q_99JCNhHWGcDNdG4ejElJRZDG64GNBicpWviFqTHu8')
  }
}
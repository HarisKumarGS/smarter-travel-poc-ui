
import { Component, OnInit } from '@angular/core';
import { BotService } from 'src/app/services/bot.service';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.scss']
})
export class ChatbotComponent implements OnInit {

  confirmIntent = false;
  confirmIntentIndex;
  messages = [];
  loading = false;
  randomPics = [];
        


  // Random ID to maintain session with server
  sessionId = Math.random().toString(36).slice(-5);

  constructor(private bot : BotService) { }

  ngOnInit() {
    this.bot.getRandomPostPic().subscribe((result: any) => {
      this.randomPics = result;
    })
    this.addBotMessage('Hi, Chris. I have found amazing options for your stay. Share us your Destination to continue booking', 'text');
    const userId = uuidv4();
    this.bot.startSession(userId).subscribe((result) => {
      localStorage.setItem('sessionId', result['sessionId']);
      localStorage.setItem('userId', userId);
      localStorage.setItem('location', 'LA');
    },err => {
      console.log('Error in Creating Session', err);
    });
  }

  async scrapMetaData() { 
  }

  handleUserMessage(event) {
    this.removeExistingResponseElements();
    const text = event.message;
    this.addUserMessage(text);

    this.loading = true;
    this.bot.postUserMessage(text).subscribe((result) => {
      this.loading = false;
      const quickActions = [];
      if(result['message']) {
        this.addBotMessage(result['message'], quickActions);
        if (result['dialogState'] === 'ConfirmIntent') {
          this.confirmIntentIndex = this.messages.length;
          this.confirmIntent = true;
        }
      }
    })
  }

  addUserMessage(text) {
    this.messages.push({
      text,
      sender: 'You',
      reply: true,
      date: new Date(),
    });

    if (this.confirmIntent) {
        this.bot.getBlogsAndPosts('India').subscribe((result) => {
          const text = `Thanks for your Reservation. Kindly check your mail for details. Check out our new deals and blogs on your Destination`;
          this.addBotMessage(text, 'text', [], [result['blogs'], result['deals']]);
        })
    }
  }

  addBotMessage(text, type, quickActions?, blogposts?) {
    this.messages.push({
      text,
      type: type,
      sender: 'Nomad',
      avatar: '/assets/bot-avatar.jpg',
      date: new Date()
    });

    if(quickActions && quickActions.length !== 0) {
      const index = this.messages.length - 1;
      let checkExist = setInterval(() => {
        if (document.getElementById(`${index}-Nomad`)) {
           this.createResponseElements(index, quickActions);
           clearInterval(checkExist);
        }
     }, 100); // check every 100ms   
    }

    if(blogposts && blogposts.length !== 0) {
      const index = this.messages.length - 1;
      let checkExist = setInterval(() => {
        if (document.getElementById(`${index}-Nomad`)) {
           this.createBlogPosts(index, blogposts);
           clearInterval(checkExist);
        }
     }, 100); // check every 100ms   
    }
  }

  createBlogPosts(index, blogposts) {
    let count = 0;
    for(let item of blogposts) {
      for(let post of item) {
        count++;
        let elem:Element = document.getElementById(`${index}-Nomad`);
        const href = document.createElement('a');
        href.href = post;
        href.target = '_blank';
        const cardContainer = document.createElement('div');
        cardContainer.className = 'post-card-container'
        const imageContainer = document.createElement('div');
        imageContainer.className = 'post-card-image-container'
        const linkContainer = document.createElement('div');
        linkContainer.className = 'link-container'
        const image = document.createElement('img');
        image.src = this.randomPics[count].urls.small;
        image.className = 'post-card-image'
        linkContainer.innerHTML = post;
        imageContainer.appendChild(image);
        cardContainer.appendChild(imageContainer);
        cardContainer.appendChild(linkContainer);
        href.appendChild(cardContainer);
        elem.parentNode.appendChild(href);
      }
    }
  }

  createResponseElements(index, quickActions) {
    for(let action of quickActions) {
      let elem:Element = document.getElementById(`${index}-Nomad`);
      const button = document.createElement('button');
      button.id = action;
      button.onclick = (event) => {
        let element = event.target as Element;
        this.handleUserMessage({message :element.id});
      }
      button.className = 'response-elements response-buttons';
      button.innerHTML = action;
      elem.parentNode.appendChild(button);
    }
  }

  removeExistingResponseElements() {
    if (document.getElementsByClassName('response-elements') && document.getElementsByClassName('response-elements').length !== 0) {
      const elements = Array.from(document.getElementsByClassName('response-elements'));
      for(let option of elements) {
        option.parentNode.removeChild(option);
      }
    }
  }
}

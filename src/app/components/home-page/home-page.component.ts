import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  typeOfPlace = '';
  travellingForWork = '';
  noOfTravellers = '1';
  budget = ''
  weekendGetaways = [
    {
      image: 'travel-1.jpg',
      title: '10 Best Places to visit in New Zealand',
      category: 'CITIES'
    },
    {
      image: 'travel-4.jpg',
      title: '10 Cheap Tropical Vacations to take in 2021',
      category: 'BEACH'
    },
    {
      image: 'travel-2.jpg',
      title: '10 Best Food Cities around America',
      category: 'FOOD & DRINK'
    },
    {
      image: 'travel-3.jpg',
      title: 'Block Off your calendar, Win a vacation',
      category: 'AIR TRAVEL'
    },
    {
      image: 'travel-5.jpg',
      title: '5 Amazing Vacation Rentals inside National Parks',
      category: 'OUTDOOR'
    }
  ]
  featuredDestinations = [
      {
        image: 'travel-1.jpg',
        title: 'Mythical Places Really Exist',
        category: 'CITIES'
      },
      {
        image: 'travel-4.jpg',
        title: 'Bucket List Destinations',
        category: 'BEACH'
      }
  ]
  topPicks = [
    {
      image: 'travel-6.jpg',
      title: 'Best Travel Destinations',
      category: 'CITIES'
    },
    {
      image: 'travel-5.jpg',
      title: 'Secret Villages in India',
      category: 'BEACH'
    }
]

  constructor() { }

  ngOnInit(): void {
  }

  onPreferenceSaved() {
    // console.log(this.typeOfPlace, this.travellingForWork, this.noOfTravellers, this.budget)
  }

}
